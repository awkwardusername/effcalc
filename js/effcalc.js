/**
 * Created by REDFOX Wizpad on 10/19/13.
 */

$(function () {
    // your programming goes here
    $('#computeForm').change(computeEffort);
    $('#linesOfCode').keyup(computeEffort);

    $('#dateToday').html(printToday());

    function computeEffort() {
        var a, b, c, d, linesOfCode = 1, projectType = "organic";

        linesOfCode = $('#linesOfCode').val() / 1000;

        switch ($('input[name="projectType"]:checked').val()) {
            case "organic":
                a = 2.4;
                b = 1.05;
                c = 2.5;
                d = 0.38;
                break;
            case "semidetached":
                a = 3.0;
                b = 1.12;
                c = 2.5;
                d = 0.35;
                break;
            case "embedded":
                a = 3.6;
                b = 1.20;
                c = 2.5;
                d = 0.32;
                break;
        }

        effort = a * Math.pow(linesOfCode, b);
        devTime = c * Math.pow(effort, d);
        peopleReq = effort / devTime;

        if(linesOfCode > 0)
            $('#monthsReq').html(Math.ceil(devTime));
        else
            $('#monthsReq').html('--');

        if(linesOfCode > 0)
            $('#devsReq').html(Math.ceil(peopleReq));
        else
            $('#devsReq').html('--');
    }

    function printToday() {
        var today = new Date();
        return today.getFullYear();
    }
});