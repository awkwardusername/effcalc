<?php
    /**
     * Created by PhpStorm.
     * User: REDFOX Wizpad
     * Date: 10/19/13
     * Time: 2:44 AM
     */

    class Calculator extends CI_Controller {
        public function __construct() {
            parent::__construct();

            $this->load->helper('url');
        }

        public function index() {
            $data['title'] = 'Calculator';

            $this->load->view('templates/header', $data);
            $this->load->view('pages/calculator', $data);
            $this->load->view('templates/footer', $data);
        }
    }