</div>
<div id="push"></div>
</div>

<!-- from #wrap -->
</div>

<div id="damnedfooter">
    <div class="container">
        <p class="muted credit">Cooked in <a href="http://getbootstrap.com">Bootstrap</a> and <a href="http://ellislab.com/codeigniter">CodeIgniter</a>. &copy; <a href="http://pup.edu.ph">CCIS - PUP</a>, <a href="http://github.com/awkwardusername">awkwardusername</a>, <elem id="dateToday">2013</elem></p>
    </div>
</div>
<script src="<?= base_url() ?>js/jquery.min.js"></script>
<script src="<?= base_url() ?>js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/effcalc.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    var _gaq = [
        ['_setAccount', 'UA-XXXXX-X'],
        ['_trackPageview']
    ];
    (function (d, t) {
        var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
        g.src = '//www.google-analytics.com/ga.js';
        s.parentNode.insertBefore(g, s)
    }(document, 'script'));

</script>
</body>
</html>