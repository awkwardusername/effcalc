<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>EffCalc<?= (isset($title) && !empty($title) && $title != "Home" ? " &mdash; $title" : "") ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="<?= base_url() ?>css/effcalc.css" media="screen">

</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->

<!-- Add your site or application content here -->
<!-- Part 1: Wrap all page content here -->
<div id="wrap">
    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url() ?>">EffCalc</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li <? if ($title == "Home"): ?>class="active"<? endif ?>><a href="<?= base_url() ?>index.php/home">Home</a></li>
                    <li <? if ($title == "Calculator"): ?>class
                    ="active"<? endif ?>><a href="<?= base_url() ?>index.php/calculator">Calculator</a></li>
                    <li <? if ($title == "About"): ?>class="active"<? endif ?>><a href="<?= base_url() ?>index.php/about">About</a></li>
                </ul>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </div>

    <div class="container">