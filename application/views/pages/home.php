<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Project Effort Calculator</h1>

        <p>An easy to use tool for estimating the project effort using COCOMO. </p>

        <p><a class="btn btn-primary btn-lg" href="<?= base_url(); ?>index.php/calculator">Start now! &raquo;</a></p>
    </div>
</div>

<hr/>

<div class="container">
    <!-- Example row of columns -->
    <h1>What is COCOMO?</h1>
    <br/>

    <div class="row">
        <div class="col-lg-4">

            <p>The Constructive Cost Model (COCOMO) is an algorithmic software cost estimation model developed by Barry
                W. Boehm. The model uses a basic regression formula with parameters that are derived from historical
                project data and current as well as future project characteristics.</p>

        </div>
        <div class="col-lg-4">

            <p>COCOMO was first published in Boehm's 1981 book Software Engineering Economics as a model for
                estimating effort, cost, and schedule for software projects. It drew on a study of 63 projects at TRW
                Aerospace where Boehm was Director of Software Research and Technology.</p>

        </div>
        <div class="col-lg-4">

            <p> The study examined projects
                ranging in size from 2,000 to 100,000 lines of code, and programming languages ranging from assembly to
                PL/I. These projects were based on the waterfall model of software development which was the prevalent
                software development process in 1981. </p>

        </div>
    </div>