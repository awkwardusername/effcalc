<h1>Basic COCOMO</h1>
<hr/>
<p>Basic COCOMO computes software development effort (and cost) as a function of program size. Program size is expressed
    in estimated thousands of source lines of code (SLOC).</p>
<p>COCOMO applies to three classes of software projects:</p>
<dl class="dl-horizontal">
    <dt>Organic</dt>
    <dd>"small" teams with "good" experience working with "less than rigid" requirements</dd>
    <dt>Semi-detached</dt>
    <dd>"medium" teams with mixed experience working with a mix of rigid and less than rigid
        requirements
    </dd>
    <dt>Embedded</dt>
    <dd>developed within a set of "tight" constraints. It is also combination of organic and
        semi-detached projects.(hardware, software, operational, ...)
    </dd>
</dl>
<p>The basic COCOMO equations take the form</p>
<dl class="dl-horizontal">
    <dd><b>Effort Applied (E)</b> = a<sub>b</sub>(KLOC)<sup>b<sub>b</sub></sup> <b>[person-months]</b></dd>
    <dd><b>Development Time (D)</b> = c<sub>b</sub>(Effort Applied)<sup>d<sub>b</sub></sup> <b>[months]</b></dd>
    <dd><b>People required (P)</b> = Effort Applied / Development Time <b>[count]</b></dd>
</dl>

<p>where, <b>KLOC</b> is the estimated number of delivered lines (expressed in thousands ) of code for project. The
    coefficients <i>a<sub>b</sub></i>, <i>b<sub>b</sub></i>, <i>c<sub>b</sub></i> and <i>d<sub>b</sub></i> are given in
    the following table:</p>

<table class="table table-hover table-striped">
    <tbody>
    <tr>
        <th>Software project</th>
        <th><i>a</i><sub><i>b</i></sub></th>
        <th><i>b</i><sub><i>b</i></sub></th>
        <th><i>c</i><sub><i>b</i></sub></th>
        <th><i>d</i><sub><i>b</i></sub></th>
    </tr>
    <tr>
        <td>Organic</td>
        <td>2.4</td>
        <td>1.05</td>
        <td>2.5</td>
        <td>0.38</td>
    </tr>
    <tr>
        <td>Semi-detached</td>
        <td>3.0</td>
        <td>1.12</td>
        <td>2.5</td>
        <td>0.35</td>
    </tr>
    <tr>
        <td>Embedded</td>
        <td>3.6</td>
        <td>1.20</td>
        <td>2.5</td>
        <td>0.32</td>
    </tr>
    </tbody>
</table>

<p>Basic COCOMO is good for quick estimate of software costs. However it does not account for differences in hardware
    constraints, personnel quality and experience, use of modern tools and techniques, and so on.</p>

<hr/>
<h2>Further reading</h2>
<ul>
    <li>Kemerer, Chris F. (May 1987). "An Empirical Validation of Software Cost Estimation Models". Communications of
        the ACM 30 (5): 416–42.
    </li>
</ul>