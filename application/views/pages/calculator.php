<h1>EffCalc 0.1a</h1>
<p>Just follow the instructions below at the <strong>Input</strong> pane to calculate the estimated effort required to accomplish your
    project. It will automatically compute the results at the <strong>Results</strong> pane.</p>
<hr/>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h3>Input Pane</h3>
            <form id="computeForm" role="form">
                <div class="form-group">
                    <label for="linesOfCode">How many lines of code does your project has?</label>
                    <input type="text" class="form-control" id="linesOfCode" placeholder="Input an integer, eg. 50000">
                </div>

                <hr/>

                <div class="form-group">
                    <label for="typeOfProject">What type of project are you doing?</label>

                    <div class="radio">
                        <label>
                            <input type="radio" name="projectType" id="projectOrganic" value="organic" checked>
                            <strong>Organic</strong> - "small" teams with "good" experience working with "less than
                            rigid" requirements
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="projectType" id="projectSemiDetached" value="semidetached">
                            <strong>Semi-detached</strong> - "medium" teams with mixed experience working with a mix of
                            rigid
                            and less than rigid requirements
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="projectType" id="projectEmbedded" value="embedded">
                            <strong>Embedded</strong> - developed within a set of "tight" constraints. It is also
                            combination of organic and semi-detached projects.(hardware, software, operational, ...)
                        </label>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4">
            <h3 class="text-center">Results Pane</h3>
            <table class="table">
                <thead>
                <tr>
                    <th class="text-center">Months to complete</th>
                    <th class="text-center">Developers required</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td><p class="jumbotron text-center" id="monthsReq">--</p></td>
                    <td><p class="jumbotron text-center" id="devsReq">--</p></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>