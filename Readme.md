# EffCalc

A Project Effort Estimation System designed to easily estimate effort for projects using the [COCOMO model](https://sites.google.com/a/irri.org/oryza2000).

## Involved

* Mark Jayson Fuentes

## System Requirements
* PHP 5.3+
* Apache 2.2
* Mysql 5.5+

## Installation

* Download and run this [Mysql database script](http://sdrv.ms/) at your database server.
* Clone this repo to your webroot
* Edit {APP_ROOT}/application/config/database.php to configure database.
* Drink More Coffee